<meta http-equiv='X-UA-Compatible' content='IE=edge'>
<meta name='viewport' content='width=device-width, initial-scale=1.0'>
<link rel='stylesheet' type='text/css' href='css/uikit.css'>
<link rel='stylesheet' type='text/css' href='css/uikit-rtl.css'>
<link rel='stylesheet href='css/princessSophia.blade.css'/>
<link rel='stylesheet' type='text/css' href='css/style.css'>
<br><br>
<center>	
<H2>SUA FICHA DE INSCRIÇÃO</H2><br>

<table class="uk-table uk-table-divider" border="1">
	<tr>
		<th>Link do Youtube</th>
		<th>Telefone</th>
		<th>Istrumento</th>
		<th>Concurso</th>
		<th>Cep</th>
		<th>Rua</th>
		<th>Bairro</th>
		<th>Cidade</th>
		<th>Estado</th>
		<th>IBGE</th>
	</tr>

<?php $__currentLoopData = $participantesC; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pC): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<?php $__currentLoopData = $ficha; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $fichaA): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<?php 
			$id=\Auth::User()->id;
			$idficha=$fichaA->getId();
			$idC=$_GET['idC'];
		?>
		
		<?php if($pC->getIdParticipante()==$id && $pC->getIdConcurso()==$idC && $pC->getIdFicha()==$idficha): ?>	
		<tr>
			<td><?php echo e($fichaA->getLinkYoutube()); ?></td>
			<td><?php echo e($fichaA->getTelefone()); ?></td>
			<td><?php echo e($fichaA->getInstrumento()); ?></td>
			<td><?php echo e($idC); ?></td>
			<td><?php echo e($fichaA->getCep()); ?></td>
			<td><?php echo e($fichaA->getRua()); ?></td>
			<td><?php echo e($fichaA->getBairro()); ?></td>
			<td><?php echo e($fichaA->getCidade()); ?></td>
			<td><?php echo e($fichaA->getEstado()); ?></td>
			<td><?php echo e($fichaA->getIbge()); ?></td>
		</tr>
		<?php endif; ?>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</table>
<br><br>

	<a href="/Concursos"><button class="uk-button uk-button-default">Voltar para Concursos</button></a>
	
</center>
<?php /* /home/lucas/Desktop/Concurso/resources/views/mostrarFicha.blade.php */ ?>