<meta http-equiv='X-UA-Compatible' content='IE=edge'>
<meta name='viewport' content='width=device-width, initial-scale=1.0'>
<link rel='stylesheet' type='text/css' href='css/uikit.css'>
<link rel='stylesheet' type='text/css' href='css/uikit-rtl.css'>
<link rel='stylesheet href='css/princessSophia.blade.css'/>
<link rel='stylesheet' type='text/css' href='css/style.css'>
<br><br>
<center>	
<H2>SUA FICHA DE INSCRIÇÃO</H2><br>

<table class="uk-table uk-table-divider" border="1">
	<tr>
		<th>Link do Youtube</th>
		<th>Telefone</th>
		<th>Istrumento</th>
		<th>Concurso</th>
		<th>Cep</th>
		<th>Rua</th>
		<th>Bairro</th>
		<th>Cidade</th>
		<th>Estado</th>
		<th>IBGE</th>
	</tr>

@foreach($participantesC as $pC)
	@foreach($ficha as $fichaA)
		<?php 
			$id=\Auth::User()->id;
			$idficha=$fichaA->getId();
			$idC=$_GET['idC'];
		?>
		
		@if($pC->getIdParticipante()==$id && $pC->getIdConcurso()==$idC && $pC->getIdFicha()==$idficha)	
		<tr>
			<td>{{$fichaA->getLinkYoutube()}}</td>
			<td>{{$fichaA->getTelefone()}}</td>
			<td>{{$fichaA->getInstrumento()}}</td>
			<td>{{$idC}}</td>
			<td>{{$fichaA->getCep()}}</td>
			<td>{{$fichaA->getRua()}}</td>
			<td>{{$fichaA->getBairro()}}</td>
			<td>{{$fichaA->getCidade()}}</td>
			<td>{{$fichaA->getEstado()}}</td>
			<td>{{$fichaA->getIbge()}}</td>
		</tr>
		@endif
	@endforeach
@endforeach
</table>
<br><br>

	<a href="/Concursos"><button class="uk-button uk-button-default">Voltar para Concursos</button></a>
	
</center>