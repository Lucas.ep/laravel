<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/Concursos','Concurso@mostrar');
Route::get('/Concursos','Concurso@mostrarConcursos')->name('Concursos');
Route::get('/Concursos/editarConc/{concursos}','Concurso@mostrarE')->name('paginaE');
Route::get('/Concursos/editarConc/atualizar/{concursos}','Concurso@atualizarC')->name('editarE');
Route::get('/AdicionarConcurso','Concurso@padicionarC');
Route::post('/AdicionarConcurso/adicionando','Concurso@adicionandoC')->name('adicionando');
Route::get('/Concursos/apagar/{concursos}','Concurso@apagarC')->name('apagarC');
Route::get('/registrarJurados','JuradosController@pregistrarJ');
Route::post('/registrarJurados/registrando','JuradosController@registrarJurados')->name('registrarJurados');

Route::get('/mostrarJurados','JuradosController@mostrarJurados');
Route::post('/mostrarJurados/selecionarJurados','JuradosConcurso@selecionarJurados')->name('selecionarJ');
Route::get('/fazerInscricao/{participantes}','JuradosController@pInscricao')->name('pinscricao');

Route::post('/Concursos/Inscrever','ParticipantesConcurso@inserir')->name('inscreverC');

Route::post('/inscricao/{participantes}','Participantes@inscricao')->name('inscricao');
Route::get('/mostrarFicha','Participantes@mostrarFicha')->name('mostrarFicha');
Route::get('/mostrarParticipantes', 'Participantes@mostrarParticipantes');

Route::get('/votarParticipante','Participantes@pVotar')->name('pvotar');
Route::post('/votarParticipante/votar','Notas@votar')->name('votar');
Route::get('/mostrarNotas','Notas@pNotas');
Route::get('/mostrarNotas','Notas@mostrarNotas')->name('mostrarN');

Route::get('/selecionarV/{idPc}','ParticipantesConcurso@selecionarV')->name('selecionarV');
Route::get('/editarF/{participantes}','Participantes@editarF')->name('editarF');