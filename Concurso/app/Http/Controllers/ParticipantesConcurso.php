<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ParticipantesConcurso extends Controller
{
    public function inserir(Request $request){
    	\App\ParticipantesConcurso::create($request->all());
    	return back();
    }
    public function selecionarV(Request $request,$id){
    	$pc=\App\ParticipantesConcurso::where('id', $id)->update(['votacao'=>2]);
    	return back();

    }

}
