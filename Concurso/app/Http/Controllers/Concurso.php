<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Concurso extends Controller
{
    public function mostrar(){
    	return view('concursos');
    }
    public function mostrarConcursos(){
    	$concursos=\App\Concurso::all();
        $mostrarJ=\App\JuradosConcurso::all();
        $mostrar=\App\ParticipantesConcurso::all();
    	return view('concursos',compact('concursos','mostrar','mostrarJ'));
    }
    public function mostrarE(\App\Concurso $concursos){
    	return view('paginaE', compact('concursos'));
    }
    public function atualizarC(Request $request, \App\Concurso $concursos){
        $concursos->update($request->all());
        return back();
    }
    public function padicionarC(){
        return view('padicionar');
    }
    public function adicionandoC(Request $request){
        \App\Concurso::create($request->all());
        return back();
    }
    public function apagarC(\App\Concurso $concursos){
        $concursos->delete();
        return back();
    }
    public function mostrarConcursosInsc(){
        $concursos=\App\Concurso::all();
        return view('inscricao',compact('concursos'));
    }
    
}
