<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantesConcursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participantes_concursos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_concurso');
            $table->integer('id_participante');
            $table->integer('id_ficha');
            $table->integer('votacao')->default(1);
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participantes_concursos');
    }
}
