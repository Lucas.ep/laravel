<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('fidelidadeE');
            $table->integer('qualidade');
            $table->integer('dificuldade');
            $table->integer('leituraP');
            $table->integer('sonoridade');
            $table->integer('presenca');
            $table->integer('precisao');
            $table->integer('musicalidade');
            $table->integer('id_participante');
            $table->integer('id_jurado');
            $table->integer('id_concurso');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notas');
    }
}
